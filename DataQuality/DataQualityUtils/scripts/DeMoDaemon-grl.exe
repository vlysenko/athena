#!/bin/bash
# Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
# Author : Benjamin Trocme (CNRS/IN2P3 - LPSC Grenoble) - 2017 - 2022
#
# Script derived from DemoDaemon.exe without update of the run list and 
# production of weekly reports.
# Mainly meant to be used for the update of the year stats for GRL runs. 
# As the GRL runs are updated only every 1-2 weeks, no cron job defined. 
# The user has to run it interactively in an atlasdqm lxplus console.
# 
# Arguments:
# - $1 : directory when to run the daemon (a priori ~atlasdqm/w1/DeMo)
# - $2 : DeMo year
# - $3 : DeMo tag
# - $4 (optional) : if chosen as resetYS, reset the year stats
##################################################################

rundir=$1
year=$2
tag=$3

if [ "$#" == "4" ];
then
    option=$4
else
    option="NoOption"
fi

date=`date`
echo "Date is ${date}"
cd ${rundir}
echo "Now in ${rundir}"
export AtlasSetup=/afs/cern.ch/atlas/software/dist/AtlasSetup
source $AtlasSetup/scripts/asetup.sh Athena,22.0.81

systems='Pixel SCT TRT LAr Tile MDT TGC RPC Trig_L1 Trig_HLT Lumi Global ALFA AFP LUCID ZDC IDGlobal BTag CaloCP MuonCP '

if [ $option == "resetYS" ];
then
    echo "Warning : I am reseting the year stats"
else
    echo "I am updating the year stats"
fi


for system in $systems
do
    echo "====================================================================================="
    echo "====================================================================================="
    echo "Processing "${system}
    echo "====================================================================================="
    echo "====================================================================================="
    

    # Script for updating the year stats
    if [ $option == "resetYS" ];
    then
	cmd="python DeMoUpdate.py -b -y ${year} -t ${tag} -s ${system} --resetYearStats --updateYearStats"
    else
	cmd="python DeMoUpdate.py -b -y ${year} -t ${tag} -s ${system} --skipAlreadyUpdated --updateYearStats"
    fi
    log="YearStats-${system}/${year}/${tag}/daemon-grl.out"
    echo "** ${cmd} **"
    eval "rm -f ${log}"
    eval "${cmd} &> ${log}"

    # Display of the year stats plots
    cmd="python DeMoStatus.py -b -s ${system} -y ${year} -t ${tag} --yearStats --savePlots"
    log="YearStats-${system}/${year}/${tag}/daemon-YSplots.out"
    echo "** ${cmd} **"
    eval "rm -f ${log}"
    eval "${cmd} &> ${log}"

    # Producing the defect recap
    cmd="python DeMoScan.py -b -y ${year} -t ${tag} -s ${system} --recapDefects"
    log="YearStats-${system}/${year}/${tag}/daemon-recapDefects.out"
    echo "** ${cmd} **"
    eval "rm -f ${log}"
    eval "${cmd} &> ${log}"
    
done

echo "Derive ATLAS data loss"
cmd="python DeMoAtlasDataLoss.py -b -y ${year} -t ${tag}"
eval ${cmd}

echo "Generate webpage"
cmd="python /afs/cern.ch/user/a/atlasdqm/w1/DeMo/DeMoGenerateWWW.py"
eval ${cmd}
