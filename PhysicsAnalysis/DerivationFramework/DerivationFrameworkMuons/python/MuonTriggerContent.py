# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

MuonTriggerContent = [
"HLT_xAOD__MuonContainer_MuonEFInfo",
"HLT_xAOD__MuonContainer_MuonEFInfoAux.",
"HLT_xAOD__MuonContainer_MuonEFInfo_FullScan",
"HLT_xAOD__MuonContainer_MuonEFInfo_FullScanAux.",
"HLT_xAOD__TrackParticleContainer_InDetTrigTrackingxAODCnv_Muon_EFID",
"HLT_xAOD__TrackParticleContainer_InDetTrigTrackingxAODCnv_Muon_EFIDAux.",
"HLT_Muons_RoI",   "HLT_Muons_RoIAux.",
"HLT_MuonsIso",    "HLT_MuonsIsoAux.",
"HLT_MuonsCB_RoI", "HLT_MuonsCB_RoIAux.",
"HLT_MuonsCB_LRT", "HLT_MuonsCB_LRTAux.",
"HLT_Muons_FS",    "HLT_Muons_FSAux.",
"HLT_MuonsCB_FS",  "HLT_MuonsCB_FSAux.",
"LVL1MuonRoIs",    "LVL1MuonRoIsAux.",
]

