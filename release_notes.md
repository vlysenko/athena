This sweep contains the following MRs:
 * !68717 DataQualityInterfaces - fix lark test for string parameters ~DQ
 * !68638 Add BRAN to ZDC geometry description ~ForwardDetectors, ~Simulation
